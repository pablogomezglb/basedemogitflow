﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BaseDemoGitFlow.Core
{
    public class Cuisine
    {
        public int CuisineId { get; set; }

        [Required, MaxLength(20)]
        public string Name { get; set; }

        public string ImageFile { get; set; }

        public IList<Restaurant> Restaurants { get; set; } = new List<Restaurant>();
    }
}
