using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaseDemoGitFlow.Core;
using BaseDemoGitFlow.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BaseDemoGitFlow.UI.Pages
{
    public class DetailModel : PageModel
    {
        private readonly IRestaurantData _restaurantData;

        public Restaurant Restaurant { get; set; }

        public string BaseUrl { get; set; }

        [TempData]
        public string Message { get; set; }

        public DetailModel(IRestaurantData restaurantData)
        {
            _restaurantData = restaurantData;
        }

        public IActionResult OnGet(int restaurantId)
        {
            BaseUrl = string.Format($"{Request.Scheme}://{Request.Host}{Request.PathBase}");

            Restaurant = _restaurantData.GetById(restaurantId);

            return Page();
        }
    }
}
