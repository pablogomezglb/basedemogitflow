﻿using BaseDemoGitFlow.Core;
using BaseDemoGitFlow.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BaseDemoGitFlow.UI.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IRestaurantData _restaurantData;
        private readonly ILogger<IndexModel> _logger;

        [BindProperty(SupportsGet = true)]
        public string SearchTerm { get; set; }

        public string Message { get; set; }

        public IEnumerable<Restaurant> Restaurants { get; set; }

        public IndexModel(IRestaurantData restaurantData, ILogger<IndexModel> logger)
        {
            _restaurantData = restaurantData;
            _logger = logger;
        }

        public void OnGet()
        {
            if (string.IsNullOrEmpty(SearchTerm))
            {
                Restaurants = _restaurantData.GetAll();
            }
            else
            {
                Restaurants = _restaurantData.GetRestaurantsByName(SearchTerm); 
            }
        }
    }
}
