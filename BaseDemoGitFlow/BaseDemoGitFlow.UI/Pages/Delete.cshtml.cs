using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaseDemoGitFlow.Core;
using BaseDemoGitFlow.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BaseDemoGitFlow.UI.Pages
{
    public class DeleteModel : PageModel
    {
        private readonly IRestaurantData _restaurantData;

        public Restaurant Restaurant { get; set; }

        public DeleteModel(IRestaurantData restaurantData)
        {
            _restaurantData = restaurantData;
        }

        public IActionResult OnGet(int restaurantId)
        {
            Restaurant = _restaurantData.GetById(restaurantId);

            if (Restaurant == null)
            {
                return RedirectToPage("./NotFound");
            }
            else
            {
                return Page(); 
            }
        }

        public IActionResult OnPost(int restaurantId)
        {
            Restaurant = _restaurantData.GetById(restaurantId);

            if (Restaurant == null)
            {
                return RedirectToPage("./NotFound");
            }

            _restaurantData.Delete(restaurantId);
            TempData["Message"] = $"{Restaurant.Name} deleted !!";

            return RedirectToPage("./Index");
        }
    }
}
