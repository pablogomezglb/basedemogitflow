using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaseDemoGitFlow.Core;
using BaseDemoGitFlow.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BaseDemoGitFlow.UI.Pages
{
    public class EditModel : PageModel
    {
        private readonly IRestaurantData _restaurantData;
        private readonly ICuisineData _cuisineData;

        [BindProperty]
        public Restaurant Restaurant { get; set; }

        [BindProperty]
        public int CuisineId { get; set; }

        public SelectList Cuisines { get; set; }

        public EditModel(IRestaurantData restaurantData, ICuisineData cuisineData)
        {
            _restaurantData = restaurantData;
            _cuisineData = cuisineData;
        }

        public IActionResult OnGet(int? restaurantId)
        {
            Cuisines = new SelectList(_cuisineData.GetAll(), "CuisineId", "Name");

            if (restaurantId.HasValue)
            {
                Restaurant = _restaurantData.GetById(restaurantId.Value);
            }
            else
            {
                Restaurant = new Restaurant();
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Restaurant.CuisineId = CuisineId;

            if (Restaurant.Id > 0)
            {
                Restaurant = _restaurantData.Update(Restaurant);
                TempData["Message"] = "Restaurant Updated !";
            }
            else
            {
                Restaurant = _restaurantData.Add(Restaurant);
                TempData["Message"] = "Restaurant Saved !";
            }

            return RedirectToPage("./Detail", new { restaurantId = Restaurant.Id });
        }
    }
}
