﻿using BaseDemoGitFlow.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace BaseDemoGitFlow.Data.Interfaces
{
    public interface ICuisineData
    {
        IEnumerable<Cuisine> GetAll();

        IEnumerable<Cuisine> GetCuisinesByName(string name);

        Cuisine GetById(int id);

        Cuisine Add(Cuisine newCuisine);

        Cuisine Update(Cuisine updatedCuisine);

        Cuisine Delete(int id);

        int Commit();
    }
}
