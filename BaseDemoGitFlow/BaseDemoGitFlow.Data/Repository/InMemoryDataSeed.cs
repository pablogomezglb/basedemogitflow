﻿using BaseDemoGitFlow.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace BaseDemoGitFlow.Data.Repository
{
    public static class InMemoryDataSeed
    {
        public static void SeedCuisine(ApplicationDbContext applicationDbContext)
        {
            List<Cuisine> cuisines = new List<Cuisine>()
            {
               new Cuisine {CuisineId = 1, ImageFile = "/images/Arabic.jpg", Name = "Arabic" },
               new Cuisine {CuisineId = 2, ImageFile = "/images/Bakery.jpg", Name = "Bakery" },
               new Cuisine {CuisineId = 3, ImageFile = "/images/Chinese.jpg", Name = "Chinese" },
               new Cuisine {CuisineId = 4, ImageFile = "/images/FastFood.jpg", Name = "Fast Food" },
               new Cuisine {CuisineId = 5, ImageFile = "/images/Grill.jpg", Name = "Grill" },
               new Cuisine {CuisineId = 6, ImageFile = "/images/Indian.jpg", Name = "Indian" },
               new Cuisine {CuisineId = 7, ImageFile = "/images/Italian.jpg", Name = "Italian" },
               new Cuisine {CuisineId = 8, ImageFile = "/images/Mexican.jpg", Name = "Mexican" }
            };

            applicationDbContext.AddRange(cuisines);

            applicationDbContext.SaveChanges();
        }

        public static void SeedRestaurant(ApplicationDbContext applicationDbContext)
        {
            List<Restaurant> restaurants = new List<Restaurant>()
            {
               new Restaurant {Id = 1, Name = "Tabun", Location = "Medellín", Address = "Cra. 33 #7-99, El Poblado", OpenHours = "12:00 - 22:00", CuisineId = 1 },
               new Restaurant {Id = 2, Name = "Carlo's", Location = "New Jersey", Address = "95 Washington St, Hoboken", OpenHours = "09:00 - 21:00", CuisineId = 2 },
               new Restaurant {Id = 3, Name = "Great NY Noodletown", Location = "Manhattan", Address = "28 Bowery, New York", OpenHours = "10:00 - 22:00", CuisineId = 3 }
            };

            applicationDbContext.AddRange(restaurants);

            applicationDbContext.SaveChanges();
        }
    }
}
