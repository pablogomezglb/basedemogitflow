﻿using BaseDemoGitFlow.Core;
using BaseDemoGitFlow.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseDemoGitFlow.Data.Repository
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public InMemoryRestaurantData(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public Restaurant Add(Restaurant newRestaurant)
        {
            _applicationDbContext.Add(newRestaurant);
            Commit();

            return newRestaurant;
        }

        public int Commit()
        {
            return _applicationDbContext.SaveChanges();
        }

        public Restaurant Delete(int id)
        {
            Restaurant restaurant = GetById(id);

            if (restaurant != null)
            {
                _applicationDbContext.Restaurants.Remove(restaurant);
                Commit();
            }

            return restaurant;
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _applicationDbContext.Restaurants.Include("Cuisine").AsEnumerable();
        }

        public Restaurant GetById(int id)
        {
            return _applicationDbContext.Restaurants.Where(r => r.Id == id).Include("Cuisine").FirstOrDefault();
        }

        public int GetCountOfRestaurants()
        {
            return _applicationDbContext.Restaurants.Count();
        }

        public IEnumerable<Restaurant> GetRestaurantsByName(string name)
        {
            return _applicationDbContext.Restaurants.Where(r => r.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase)).Include("Cuisine").OrderBy(r => r.Name);
        }

        public Restaurant Update(Restaurant updatedRestaurant)
        {
            var restaurant = _applicationDbContext.Restaurants.Attach(updatedRestaurant);

            restaurant.State = EntityState.Modified;
            Commit();

            return updatedRestaurant;
        }
    }
}
