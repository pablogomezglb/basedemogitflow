﻿using BaseDemoGitFlow.Core;
using BaseDemoGitFlow.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseDemoGitFlow.Data.Repository
{
    public class InMemoryCuisineData : ICuisineData
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public InMemoryCuisineData(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public Cuisine Add(Cuisine newCuisine)
        {
            _applicationDbContext.Add(newCuisine);

            return newCuisine;
        }

        public int Commit()
        {
            return _applicationDbContext.SaveChanges();
        }

        public Cuisine Delete(int id)
        {
            Cuisine cuisine = GetById(id);

            if (cuisine != null)
            {
                _applicationDbContext.Remove(cuisine);
            }

            return cuisine;
        }

        public IEnumerable<Cuisine> GetAll()
        {
            return _applicationDbContext.Cuisines.AsEnumerable();
        }

        public Cuisine GetById(int id)
        {
            return _applicationDbContext.Cuisines.Find(id);
        }

        public IEnumerable<Cuisine> GetCuisinesByName(string name)
        {
            return _applicationDbContext.Cuisines.Where(r => r.Name.StartsWith(name) || string.IsNullOrEmpty(name)).OrderBy(r => r.Name);
        }

        public Cuisine Update(Cuisine updatedCuisine)
        {
            var cuisine = _applicationDbContext.Cuisines.Attach(updatedCuisine);

            cuisine.State = EntityState.Modified;

            return updatedCuisine;
        }
    }
}
