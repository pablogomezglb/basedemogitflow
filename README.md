## Base Demo Gitflow

This is a small website where you can create, list, edit and delete Restaurants references with a very basic information.

The purpose is just to have a basic implememtation of a project that you can use for any purpose.

---

## Details

- Is a ASP.NET Core Web App
- Based on .NET Core 3.1 LTS
- UI project is based on Razor Pages
- Uses Entity Framework as ORM with InMemory implementation
